<!-- Begin Top -->
	<section class="top" data-wow-delay="0.5s">
		<div class="row collapse align-center align-middle">
			<div class="small-12 medium-3 columns text-center">
				<?php dynamic_sidebar( 'logo' ); ?>
			</div>
			<div class="small-12 medium-9 columns">
				<?php dynamic_sidebar( 'data' ); ?>
			</div>
		</div>
	</section>
	<section class="top_1" data-wow-delay="0.5s">
		<div class="row collapse align-center align-middle">
			<div class="small-12 medium-9 columns">
				<?php dynamic_sidebar( 'menu' ); ?>
			</div>
			<div class="small-12 medium-3 columns">
				<?php dynamic_sidebar( 'search' ); ?>
			</div>
		</div>
	</section>
<!-- End Top -->