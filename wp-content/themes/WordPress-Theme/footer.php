		<?php if ( is_front_page() ) : get_template_part( 'part', 'block-1' ); endif; ?>
		<?php if ( is_front_page() ) : get_template_part( 'part', 'block-2' ); endif; ?>
		<?php if ( is_front_page() ) : get_template_part( 'part', 'block-3' ); endif; ?>
		<?php if ( is_front_page() ) : get_template_part( 'part', 'block-4' ); endif; ?>
		<?php if ( is_front_page() ) : get_template_part( 'part', 'block-5' ); endif; ?>
		<?php if ( is_front_page() ) : get_template_part( 'part', 'block-6' ); endif; ?>
		<?php if ( is_front_page() ) : get_template_part( 'part', 'block-7' ); endif; ?>
		<?php if ( is_front_page() ) : get_template_part( 'part', 'block-8' ); endif; ?>
		<?php get_template_part( 'part', 'bottom' ); ?>
		<?php get_template_part( 'part', 'map' ); ?>
		<?php get_template_part( 'part', 'copyright' ); ?>
		<?php wp_footer(); ?>
	</body>
</html>