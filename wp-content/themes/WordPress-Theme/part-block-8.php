<!-- Begin Block 8 -->
	<section class="block_8" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'block_8' ); ?>
			</div>
		</div>
	</section>
<!-- End Block 8 -->