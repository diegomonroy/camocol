<!-- Begin Map -->
	<section class="map" data-wow-delay="0.5s">
		<div class="row collapse expanded">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'map' ); ?>
			</div>
		</div>
	</section>
<!-- End Map -->